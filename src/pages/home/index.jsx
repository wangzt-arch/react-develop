import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./index.css";

export default class Home extends Component {
  goto = (e) => {
    this.props.history.push(`/${e}`);
  };
  render() {
    return (
      <div className="home">
          <Link to="/home">home</Link>
          <Link to="/account">account</Link>
          <Link to="/login">login</Link>
          <Link to="/antd-view">Antd</Link>
          <Link to="/lifecycles">lifeCycles</Link>
          <Link to="/element-view">element</Link>
        {/* <div className="div-account" onClick={this.goto.bind(this, "account")}>
          go to account
        </div>
        <div className="div-home" onClick={() => this.goto("home")}>
          go to home
        </div>
        <div className="div-login" onClick={() => this.goto("login")}>
          go to login
        </div> */}
      </div>
    );
  }
}
