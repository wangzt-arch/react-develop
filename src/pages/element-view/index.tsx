import React from "react";
import { Switch } from "element-react";
import "element-theme-default";
import Home from "../home/index";
import "./index.scss";

export default function Index() {
  return (
    <div>
      <Home></Home>
      <Switch value={true} onText="" offText=""></Switch>
      <Switch value={true} onColor="#13ce66" offColor="#ff4949"></Switch>
    </div>
  );
}
